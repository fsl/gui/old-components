import {Typography} from '@mui/material'
import {Box} from '@mui/material'

export function Title(props){
  // useage
  //
  // <Title
  //    text='my title'
  //    variant='h1',     //default: 'h3'
  //    justify='center'  // default: 'center'
  // >
  // </Title>
  let text = props['text'] === undefined ? 'title' : props['text']
  let variant = props['variant'] === undefined ? 'h3' : props['variant']
  let justify = props['justify'] === undefined ? 'center' : props['justify']
  let color = props['color'] === undefined ? 'black' : props['color']
  let mt = props['mt'] === undefined ? 4 : props['mt']
  let mb = props['mb'] === undefined ? 4 : props['mb']
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: justify,
        marginTop: mt, 
        marginBottom: mb
      }}
    >
      <Typography 
        variant={variant}
        sx={{
          color: color
        }}
      >
        {text}
      </Typography>
    </Box>
    
  )
}
