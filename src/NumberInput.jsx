import React from 'react'
import {Box} from '@mui/material'
import {Typography} from '@mui/material'
import {Input} from '@mui/material'


export function NumberInput(props){
  // useage
  //
  // <NumberInput
  //    onInput=handlerFunc
  //    value=Number
  //    min=Number
  //    max=Number
  //    step=Number
  //    label='some number label'
  // </NumberInput>
  let onInput = props['onInput'] === undefined ? ()=>{} : props['onInput']
  let inputValue = props['value'] === undefined ? 0 : props['value']
  let min = props['min'] === undefined ? 0 : props['min']
  let max = props['max'] === undefined ? 1 : props['max']
  let step = props['step'] === undefined ? 0.1 : props['step']
  let label = props['label'] === undefined ? 'label' : props['label']
  let color = props['color'] === undefined ? 'black' : props['color']
  let backgroundColor = props['backgroundColor'] === undefined ? 'white' : props['backgroundColor']

  const [value, setValue] = React.useState(inputValue)

  function handleNumberInput(event){
    let value = event.target.value
    setValue(value)
    onInput(value)
  }

  return (
    <Box 
      sx={{
        display: 'flex',
        flexDirection: 'row',
        // width: '100%',
        alignItems: 'center', //vertical alignment
        marginTop: 1, 
        marginBottom: 1
      }}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          ml: 1, 
          mr: 1
        }}
      >
        <Typography sx={{color:color}}>
          { label }
        </Typography>
        <Input
          size='small'
          value={Number(value) ?? 0}
          inputProps={{
            min: min,
            max: max,
            step: step,
            type: 'number',
            style: {color: color, backgroundColor: backgroundColor}
          }}
          onInput={handleNumberInput}
        >
        </Input>
      </Box>
    </Box>

  )  
}
