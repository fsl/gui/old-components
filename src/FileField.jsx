import {Box} from '@mui/material'
import {TextField} from '@mui/material'
import {Button} from '@mui/material'
import {IconButton} from '@mui/material'
import React from 'react'
import RemoveIcon from '@mui/icons-material/Remove'

export function FileField(props){
  // useage: 
  //
  // <FileField
  //    showChooseButton=true
  //    buttonText='Choose'
  //    label='Input file'
  //    onInput=inputHandlerFunc
  //    type='file | directory'
  // >
  // </FileField>
  //
  // default showChooseButton to true if not given
  let showChooseButton = props['showChooseButton'] === undefined ? true : props['showChooseButton']
  let showRemoveButton = props['showRemoveButton'] === undefined ? false : props['showRemoveButton']
  let buttonText = props['buttonText'] === undefined ? 'Choose' : props['buttonText']
  let label = props['label'] === undefined ? 'label' : props['label']
  let onInput = props['onInput'] === undefined ? ()=>{} : props['onInput']
  let onRemove = props['onRemove'] === undefined ? ()=>{} : props['onRemove']
  let onClick = props['onClick'] === undefined ? ()=>{} : props['onClick']
  let inputType = props['type'] === undefined ? 'file' : props['type']
  let value = props['value'] === undefined ? '' : props['value']
  let mt = props['mt'] === undefined ? 1 : props['mt']
  let mb = props['mb'] === undefined ? 1 : props['mb']
  let mr = props['mr'] === undefined ? 1 : props['mr']
  let ml = props['ml'] === undefined ? 1 : props['ml']
  let color = props['color'] === undefined ? 'black' : props['color']
  let backgroundColor = props['backgroundColor'] === undefined ? 'white' : props['backgroundColor']
  let hoverColor = props['hoverColor'] === undefined ? 'white' : props['hoverColor']
  let width = props['width'] === undefined ? '99%' : props['width']
  let button
  const [fileName, setFileName] = React.useState(value)
  // let commsConfig
  
  React.useEffect(()=>{
    setFileName(value)
  }, [value])

  function handleInput(event){
    let value = event.target.value
    setFileName(value)
    onInput(value)
  }
  if (showChooseButton){
    button = 
      <Button
        variant='contained'
        style={{
          marginLeft: 24, 
          textTransform: 'none', 
        }}
        sx={{
          color: color,
          backgroundColor: backgroundColor,
          '&:hover': {
            backgroundColor: color,
            color: backgroundColor,
          },
        }}
        onClick={onClick}
      >
        {buttonText}
      </Button>
  } else {
    button = null
  }

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        marginTop: mt, 
        marginBottom: mb,
        marginRight: mr,
        marginLeft: ml,
        width: width,

      }}
    >
      <IconButton 
        component="label"
        style={{display: showRemoveButton ? '' : 'none'}}
        sx={{
          color: color,
          backgroundColor: backgroundColor,
          '&:hover': {
            backgroundColor: hoverColor,
          },
        }}
        onClick={onRemove}
        >
        <RemoveIcon/>
      </IconButton>
      <TextField 
        fullWidth
        size='small'
        label={label}
        onInput={handleInput}
        value={fileName}
        autoCapitalize='off'
        autoCorrect='off'
        autoComplete='off'
        spellCheck='false'
        variant='standard'
        InputProps={{
          style: {
            color: color,
            borderColor: `${color} !important`,
          },
        }}
        FormHelperTextProps={{
          style: {color: color},
        }}
        InputLabelProps={{
          style: {color: color},
        }}
      >
      </TextField> 

      {button}
      
    </Box>
  )
}
