import {Box} from '@mui/material'
// import { LoadingButton } from '@mui/lab'
import {Button} from '@mui/material'
import React from 'react'

export function OptionsPanel(props){
  // useage
  //
  // <OptionsPanel
  //
  // >
  // </OptionsPanel>
  const children = props['children']
  let visible = props['visible'] === undefined ? false : props['visible']
  let display = visible ? 'flex' : 'none'
  return (
    <Box
      sx={{
        display: display,
        flexDirection: 'column',
        width: '100%',
        alignItems: 'left',
        justifyContent: 'left',
        mt: 1, 
        mb: 1
      }}
    >
      { children }

    </Box>
  )
}
