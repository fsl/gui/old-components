import React from 'react'
import {Box} from '@mui/material'
import {Slider} from '@mui/material'
import {Typography} from '@mui/material'
import {Input} from '@mui/material'

export function NumberSlider(props){
  // useage
  //
  // <NumberSlider
  //    onInput=handlerFunc
  //    value=Number
  //    min=Number
  //    max=Number
  //    step=Number
  //    label='f-value'
  // </NumberSlider>
  let onInput = props['onInput'] === undefined ? ()=>{} : props['onInput']
  let inputValue = props['value'] === undefined ? 0 : props['value']
  let min = props['min'] === undefined ? 0 : props['min']
  let max = props['max'] === undefined ? 1 : props['max']
  let step = props['step'] === undefined ? 0.1 : props['step']
  let label = props['label'] === undefined ? 'label' : props['label']

  const [value, setValue] = React.useState(inputValue)

  function handleSliderInput(event){
    let value = event.target.value
    setValue(value)
    onInput(value)
  }

  function handleNumberInput(event){
    let value = event.target.value
    setValue(value)
    onInput(value)
  }

  return (
    <Box 
      sx={{
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center', //vertical alignment
        marginTop: 1, 
        marginBottom: 1
      }}
    >
      <Box
        sx={{
          display: 'flex',
          width: '100%',
          flexDirection: 'column',
          ml: 1, 
          mr: 1
        }}
      >
        <Typography>
          { label }
        </Typography>
        <Slider
          onChange={handleSliderInput}
          value={Number(value) ?? 0}
          min={min}
          max={max}
          step={step}
        >
        </Slider>
      </Box>
      <Box
        sx={{
          display: 'flex',
          marginLeft: 4,
          justifyContent: 'right'
        }}
      >
        <Input
          size='small'
          value={Number(value) ?? 0}
          inputProps={{
            min: min,
            max: max,
            step: step,
            type: 'number'
          }}
          onInput={handleNumberInput}
        >
        </Input>
      </Box>
    </Box>

  )  
}
