import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export function SelectInput(props) {
  const [value, setValue] = React.useState(props.value || 'BBR');
  let menuItems = props.options === undefined ? [{value:'value', key:'item'}] : props.options
  let label = props.label === undefined ? 'label' : props.label
  let color = props.color === undefined ? 'black' : props.color
  let backgroundColor = props.backgroundColor === undefined ? 'white' : props.backgroundColor

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <Box sx={{ minWidth: 280, display:'flex' }}>
      <FormControl fullWidth>
        <InputLabel
          sx={{color:color, width:'100%'}}
        >{label}</InputLabel>
        <Select
          value={value}
          label={label}
          onChange={handleChange}
          inputProps={{
            sx: {
              color: color,
              backgroundColor: backgroundColor
            }
          }}
        >
        {menuItems.map((item, index) => {
            return <MenuItem key={index} value={item.value}>{item.label}</MenuItem> 
        }   
        )}
        </Select>
      </FormControl>
    </Box>
  );
}