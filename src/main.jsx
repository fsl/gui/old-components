import React from 'react'
import ReactDOM from 'react-dom/client'
import { FileField } from './FileField';
import { ActionsPanel } from './ActionsPanel';
import { CheckBox } from './CheckBox';
import { CommandPreview } from './CommandPreview';
import { NiivueDisplay } from './NiivueDisplay';
import { NumberInput } from './NumberInput';
import { NumberSlider } from './NumberSlider';
import { OptionsPanel } from './OptionsPanel';
import { Title } from './Title';
import { SelectInput } from './SelectInput';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <React.Fragment>
    <FileField />
    <FileField showRemoveButton={true} />
    <ActionsPanel />
    <CheckBox />
    <CommandPreview />
    <NiivueDisplay />
    <NumberInput />
    <NumberSlider />
    <OptionsPanel />
    <Title />
    <SelectInput />
    </React.Fragment>
  </React.StrictMode>
)
