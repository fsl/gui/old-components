import {Box} from '@mui/material'
import {FormControlLabel} from '@mui/material'
import Checkbox, {checkboxClasses} from '@mui/material/Checkbox'

export function CheckBox(props){
  // useage
  //
  // <CheckBox
  //    label=
  //    checked=
  //    onChange=
  // >
  // </CheckBox>
  let label = props['label'] === undefined ? 'checkbox' : props['label']
  let checked = props['checked'] === undefined ? false : props['checked']
  const onChange = props['onChange'] === undefined ? ()=>{} : props['onChange']
  let color = props['color'] === undefined ? 'primary' : props['color']

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'left',
        marginTop: 0, 
        marginBottom: 0// TODO make margins smaller
      }}
    >                                                                                    
      <FormControlLabel                                                                      
        control={
          <Checkbox 
            checked={checked} 
            onChange={onChange}
            sx={{
              [`&, &.${checkboxClasses.checked}`]: {
                color: color,
              },
            }}
          />
          }    
        sx={{
          color: color
        }}                               
        label={label}
      />                                           
    </Box>
    
  )
}
