import {Box} from '@mui/material'
// import { LoadingButton } from '@mui/lab'
import {Button} from '@mui/material'
import React from 'react'

export function ActionsPanel(props){
  // useage
  //
  // <ActionsPanel
  //    onRun=handlerFunc
  //    moreOptions=true // default: true
  //    mt={2}  // margin top
  //    mb={2}  // margin bottom
  //    ml={2}  // margin left
  //    mr={2}  // margin right
  // >
  // </ActionsPanel>
  let onRun = props['onRun'] === undefined ? ()=>{} : props['onRun']
  let onMoreOptions = props['onMoreOptions'] === undefined ? ()=>{} : props['onMoreOptions']
  // hiding this button not implemented yet
  let showMoreOptionsButton = props['moreOptions'] === undefined ? true : props['moreOptions']
  let marginTop = props['mt'] === undefined ? 2 : props['mt']
  let marginBottom = props['mb'] === undefined ? 2 : props['mb']
  let marginLeft = props['ml'] === undefined ? 2 : props['ml']
  let marginRight = props['mr'] === undefined ? 2 : props['mr']
  let runDisabled = props['runDisabled'] === undefined ? false : props['runDisabled']
  const [running, setRunning] = React.useState(false)
  const [optionsButtonText, setOptionsButtonText] = React.useState('more options')
  let optionsButtonStateToggle = false

  const handleMoreOptionsClick = function(){
    onMoreOptions()
    if (optionsButtonText === 'more options'){
      setOptionsButtonText('less options')
    } else if (optionsButtonText === 'less options') {
      setOptionsButtonText('more options')
    }
    

  }
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        mt: marginTop, 
        mb: marginBottom
      }}
    >
      <Button
        onClick={onRun}
        variant='contained'
        // loadingIndicator='running...'
        // loading={running}
        disabled={runDisabled}
        style={{
          margin: 2
        }}
      >
        Run
      </Button>

      <Button
        onClick={handleMoreOptionsClick}
        style={{
          margin: 2,
          textTransform: 'none'
        }}
      >
        {optionsButtonText}
      </Button>

    </Box>
  )
}
