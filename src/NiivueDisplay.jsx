import { Niivue } from '@niivue/niivue'
import React from "react"
import {Box} from '@mui/material'

export function NiivueDisplay(props) {
  const canvas = React.useRef()

  let images = props['images'] === undefined ? [] : props['images']
  let height = props['height'] === undefined ? 400 : props['height']
  let width = props['width'] === undefined ? 640 : props['width']
  const [nv, setNv] = React.useState(null)

  
  React.useEffect(() => {
    let nv = new Niivue({})
    nv.attachToCanvas(canvas.current)
    const volumes = []
    for (let i = 0; i < images.length; i++) {
      volumes.push(
        images[i]
      )
    }
    if (volumes.length > 0){
      nv.loadVolumes(volumes)
    }
    setNv(nv)

  }, [])

  React.useEffect(() => {
    if (nv !== null){
      const volumes = []
      for (let i = 0; i < images.length; i++) {
        volumes.push(
          images[i]
        )
      }
      if (volumes.length > 0){
        nv.loadVolumes(volumes)
      }
    }
  }, [images])

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        marginTop: 1, 
        marginBottom: 1
      }}
    >
      <canvas ref={canvas} height={height} width={width}></canvas>
    </Box>
  )
}