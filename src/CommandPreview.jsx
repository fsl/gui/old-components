import {Typography} from '@mui/material'
import {Box} from '@mui/material'
import { Snackbar } from '@mui/material'
import React from 'react'

export function CommandPreview(props){
  const [snackOpen, setSnackOpen] = React.useState(false)
  // useage
  //
  // <CommandPreview
  //    text='my title'
  //    justify='center'  // default: 'center'
  //    onClick=handlerFunc // default is to copy text to clipboard
  // >
  // </CommandPreview>
  function defaultOnClick(event){
    // get element from event
    let element = event.target
    // get text from element
    let text = element.innerText
    // copy to clipboard
    navigator.clipboard.writeText(text)
    // show snackbar
    setSnackOpen(true)
  }
  let text = props['text'] === undefined ? '' : props['text']
  let justify = props['justify'] === undefined ? 'center' : props['justify']
  const onClick = props['onClick'] === undefined ? defaultOnClick : props['onClick']

  function handleSnackClose(event, reason){
    // close snackbar
    setSnackOpen(false)
  }
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: justify,
        marginTop: 4, 
        marginBottom: 4
      }}
    >
      <Typography
        sx={{fontFamily: 'Monospace'}}
        onClick={onClick}
      >
        {text}
      </Typography>
      <Snackbar
        open={snackOpen}
        autoHideDuration={4000}
        onClose={handleSnackClose}
        message="Copied to clipboard"
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
      </Snackbar>
    </Box>
    
  )
}
